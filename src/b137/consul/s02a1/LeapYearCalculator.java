package b137.consul.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {
    public static void main(String[] args){
        System.out.println("Leap Year Calculator\n");

        Scanner appScanner = new Scanner(System.in);

        /*System.out.println("What is your firstname?\n");
        String firstName = appScanner.nextLine();
        System.out.println("Hello, " + firstName + "!\n");*/

        // Activity: Create a program that check if a year is a leap year
        // 1999 != leap year
        // 1900 != leap year
        // 2000 = leap year
        // 2004 = leap year
        System.out.println("Input year:\n");
        int year = appScanner.nextInt();
        if(year % 4 == 0){
            if(year % 100 == 0){
                if(year % 400 == 0){
                    System.out.println("The year " + year + " is a leap year");
                } else {
                    System.out.println("The year " + year + " is not a leap year");
                }
            } else {
                System.out.println("The year " + year + " is a leap year");
            }
        } else {
            System.out.println("The year " + year + " is not a leap year");
        }
    }
}
